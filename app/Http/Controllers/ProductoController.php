<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $productos = Auth::user()->products()->orderBy('created_at', 'desc')->paginate(5);
        //dd($productos);
        return view('crud.index', [
            'productos'=> $productos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('crud.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $productos = $request->validate([
            'codigo' => 'required|unique:productos|max:10',
            'descripcion' => 'required',
            'cantidad' => 'required',
            'precio' => 'required'
        ]);
        /* $productos = new Producto();
        $productos->codigo = $request->get('codigo');
        $productos->descripcion = $request->get('descripcion');
        $productos->cantidad = $request->get('cantidad');
        $productos->precio = $request->get('precio');
        //$productos->user_id = auth()->user()->id;


        $productos->save(); */


        //$usuario = User::find(auth()->user()->id);

        Auth::user()->products()->create([
            'codigo' => $request->get('codigo'),
            'descripcion' => $request->get('descripcion'),
            'cantidad' => $request->get('cantidad'),
            'precio' => $request->get('precio'),
        ]);


        return redirect()->route('productos.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $producto = Producto::findOrFail($id);
        return view("crud.update")->with('producto',$producto);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $producto = Producto::findOrFail($id);
        $producto->codigo = $request->get('codigo');
        $producto->descripcion = $request->get('descripcion');
        $producto->cantidad = $request->get('cantidad');
        $producto->precio = $request->get('precio');

        $producto->save();

        return redirect()->route('productos.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $producto = Producto::find($id);
        $producto->delete();
        return redirect()->route('productos.index');
    }
}
