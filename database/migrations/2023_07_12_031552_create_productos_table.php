<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id()->comment('id del producto');
            $table->string('codigo')->unique()->comment('codigo del producto');
            $table->string('descripcion')->comment('descripcion del producto');
            $table->integer('cantidad')->comment('cantidad de producto');
            $table->decimal('precio')->comment('costo del producto');
            $table->foreignId('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('productos');
    }
};
