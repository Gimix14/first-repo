@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Agregar</h1>
@stop

@section('content')
<h2>Ingresar Producto</h2>
<!-- /resources/views/post/create.blade.php -->

<h1>Create Post</h1>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- Create Post Form -->
<form action="{{ route('productos.store') }}" method="POST">
    @csrf
    <div class="mb-3">
        <!-- /resources/views/post/create.blade.php -->

    <label for="codigo">Codigo</label>

    <input id="codigo"
        type="text"
        name="codigo"
        class="form-control @error('codigo') is-invalid @enderror"
        value="{{ old('codigo') }}">

    @error('codigo')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    </div>
    <div class="mb-3">
        <label for="" class="form-label">Descripcion</label>
        <input id="descripcion" name="descripcion" type="text" class="form-control" tabindex="2">
    </div>
    <div class="mb-3">
        <label for="" class="form-label">Cantidad</label>
        <input id="cantidad" name="cantidad" type="number" class="form-control" tabindex="3">
    </div>
    <div class="mb-3">
        <label for="" class="form-label">Precio</label>
        <input id="precio" name="precio" type="number" step="any" value="0.00" class="form-control" tabindex="4">
    </div>
    <a href="{{ route('productos.index') }}" class="btn btn-secondary" tabindex="5">Cancelar</a>
    <button type="submit" class="btn btn-primary" tabindex="4">Guardar</button>
</form>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');
</script>
@stop
