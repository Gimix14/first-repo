<?php

use Illuminate\Support\Facades\Route;
use App\Models\Producto;
use App\Http\Controllers\ProductoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->prefix('admin')->group(function () {
    Route::get('/dash', [ProductoController::class, 'index'])->name('dash');


    Route::resource('inventario', ProductoController::class)->names([
        'create' => 'productos.create',
        'destroy'=> 'productos.destroy',
        'edit' => 'productos.edit',
        'update' => 'productos.update',
        'store' => 'productos.store',
        'index' => 'productos.index'
    ]);

    /* Route::controller(ProductoController::class)->group(function(){
        Route::get('inventario', 'index');
        Route::get('inventario/create', 'index');
        Route::get('inventario', 'index');

    }); */

    Route::get('/crud/create', function () {
        return view('crud.create');
    });
});

