<?php

namespace App\Models;

use Egulias\EmailValidator\Parser\Comment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Producto extends Model
{
    use HasFactory;

    protected $table = "productos";
    protected $fillable = [
        'codigo',
        'descripcion',
        'cantidad',
        'precio',
        'user_id'
    ];
    public function user(): BelongsTo{
        return $this->belongsTo(User::class);
    }
}
